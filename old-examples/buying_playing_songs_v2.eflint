#

Fact publisher    // actor
Fact listener     // actor
Fact song         // asset

Fact owner-of Identified by publisher * song // which publishers own which songs?
Fact offer    Identified by listener * song  // whether an offer has been made (permission to accept offer)

Act offer-music
  Actor publisher
  Recipient listener
  Related to song
  Creates offer(listener,song) // offer can expire, hardcoding dates is not reusable so expiration ignored for now
  Holds when owner-of(publisher,song)  // publisher must own the song

// duty to pay based on accepting an offer

Fact day Identified by Int
Placeholder current-day For day

Act accept-offer 
  Actor listener
  Recipient publisher
  Related to song
  Creates duty-to-pay(listener,publisher,song,day+14) // completely redundant, see `pay` below
  Holds when offer(listener,song)

Duty duty-to-pay
  Holder listener
  Claimant publisher
  Related to song, day
  Violated when current-day > day // the current day is larger than the duedate

Act pay // alternative specification of `pay`, involving the duty
  Actor listener
  Recipient publisher
  Related to song, amount
  Terminates duty-to-pay When duty-to-pay.listener == listener
                           && duty-to-pay.song == song
                           && duty-to-pay.publisher == publisher
  Creates permission-to-play(listener,song,2) // maximum plays is 2
  Holds when duty-to-pay && amount == 5

Fact amount Identified by Int
Fact counter Identified by Int
Fact permission-to-play Identified by listener * song * counter // listener can play song counter times

Act play
  Actor listener
  Recipient publisher
  Related to song
  Terminates permission-to-play(listener,song,counter)
  Creates    permission-to-play(listener,song,counter-1) When permission-to-play(listener,song,counter)
  Holds when permission-to-play(listener,song,counter) && counter > 0



##

//you can redefine fact-types here to given them narrower scope (less instances)
//this allows you to zoom in on the case analyzed by the scenario

Fact amount Identified by 4,5
Fact counter Identified by 0..2  // simplified to a maximum of 2 times rather than 10

Fact publisher Identified by NOYS
Fact listener  Identified by Alice, Bob
Fact song      Identified by Oops, BayBeeBayBee

###

day(0).
owner-of(NOYS,song).

####

// scenario 2: Alice buys Oops, receiving a duty after accepting the offer
offer-music(NOYS,Alice,Oops).
accept-offer(Alice,NOYS,Oops).
-day(0). +day(10).  // 10 days since duty
?!Violated(duty-to-pay(Alice,NOYS,Oops,14)). // duty not yet violated
-day(10). +day(15). // 15 days since duty
?Violated(duty-to-pay(Alice,NOYS,Oops,14)).  // duty violated
pay(Alice,NOYS,Oops,5).
play(Alice,NOYS,Oops). // allowed
play(Alice,NOYS,Oops). // allowed
?!Enabled(play(Alice,NOYS,Oops)). // playing is no longer permitted

accept-offer(Alice,NOYS,Oops). // do the whole thing again
pay(Alice,NOYS,Oops,5).
play(Alice,NOYS,Oops). // allowed
play(Alice,NOYS,Oops). // allowed
?!Enabled(play(Alice,NOYS,Oops)). // playing is no longer permitted
