L. Thomas van Binsbergen -- Centrum Wiskunde & Informatica, Amsterdam

First drafts of Meat selling contract in eFLINT, example taken from
  "Symboleo: Towards a Specification Language for Legal Contracts"
  Sharifi, S., Parvizimosaed, A., Amyot, D., Logrippo, L., Mylopoulos, J. (2020). RE@Next! IEEE CS 

v1: attempt to reconstruct Symboleo example faithfully
v2: similar model, but more according to eFLINT "idioms"

testable at http://grotius.uvalight.net/eflintonline
